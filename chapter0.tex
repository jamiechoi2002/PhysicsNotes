\section{Preliminary Mathematics}

\subsection{Linear relation}

If the relation between two variables can be represented by a \textbf{linear graph} (i.e. a straight line), then two variables are said to be linearly related. Their relation is called \textbf{linear relation}.

If their relation is represented by an equation in the \textbf{slope-intercept form}:

$$ y = mx + c $$

then $m$ is the \textbf{slope} of the graph and $c$ is the \textbf{y-intercept} of the graph.

\subsection{Direct proportion}

In the equation $y = mx + c$, if the y-intercept $c$ is equal to zero, then the linear graph must pass through the \textbf{origin}. The equation becomes $y = mx$. This relation is called \textbf{}direct proportion.

If $P$ and $Q$ are directly proportional to each other, then mathematically, we may write

$$ P \propto Q $$

The following three statements are equivalent to each other.

\begin{enumerate}
    \item $P$ is directly proportional to Q (i.e. $P \propto Q$).
    \item $P = kQ$, where $k$ is a constant. ($k$ is called the proportionality constant.)
    \item The graph of $P$ and $Q$ is a straight line (linear graph) passing through the origin and the proportionality constant in the slope of the line.
\end{enumerate}

The proportionality constant $k$ usually has a physical meaning. It is the ratio of $P$ to $Q$. It also describes the \textbf{rate of change} of $P$ with respect to $Q$. That is, when $Q$ is increased by $1$ unit, the change in $P$ is actually given by $k$.

\subsection{Inverse proportion}

If $P$ is multiplied by a constant $k$, $Q$ will be divided by the same constant $k$. This relation is called \textbf{inverse proportion}.

If $P$ and $Q$ are said to be inversely proportional to each other, then

$$ PQ = k $$

where $k$ is a constant.

\textbf{Misconception:} A linear graph with negative slope \textbf{does not} represent an inverse proportion.

We see that if $P$ is \textbf{inversely proportional} to $Q$, then $P$ must be \textbf{directly proportional} to $\frac{1}{Q}$. Practically, it is more convenient to transform the graph to a linear one so that plotting is more accurate and easier.

The following statements are equivalent to each other:

\begin{enumerate}
    \item $P$ is inversely proportional to $Q$.
    \item $P$ is directly proportional to $\frac{1}{Q}$ (i.e. $P \propto \frac{1}{Q}$)
    \item $PQ = k$, where $k$ is a constant.
    \item The graph of $P$ against $\frac{1}{Q}$ is a straight line passing through the origin, with a slope $k$.
\end{enumerate}

\subsection{Scientific notation}

Scientific notation is useful because the \textbf{order of magnitudes} of the numbers are expressed more explicitly and easily seen.

\subsubsection{Commonly used metric prefixes}

\begin{tabular}{|c c c c|}
    \hline
    Prefix & Symbol & Factor & Numerically\\
    \hline
    Giga & G & $10^{9}$ & $1000000000$\\
    Mega & M & $10^{6}$ & $1000000$\\
    Kilo & k & $10^{3}$ & $1000$\\
    Milli & m & $10^{-3}$ & $0.001$\\
    Micro & $\mu$ & $10^{-6}$ & $0.000001$\\
    Nano & n & $10^{-6}$ & $0.000000001$\\
    \hline
\end{tabular}