\section{Kinematics}

\subsection{Time and length}

To study the motion of an object, the basic quantities to be measured are \textbf{time} and \textbf{length}.

The \textbf{SI units (International system of units)} of time and length are \textbf{s} and \textbf{m} respectively.

Devices used to measure time intervals in the laboratory: stop-watch, timer scaler with light gates\\
Devices used to measure lengths in the laboratory: meter rule, Vemier Caliper

\subsection{Distance traveled and displacement}

\textbf{Distance travelled} is defined as the length of the path taken.\\
\textbf{Displacement} is defined as \textbf{change in position}. It depends on \textbf{initial and final positions} only, but not on the path taken.

The displacement can be represented graphically by a straight arrow, joining from the \textbf{initial position to the final position}. The length of the arrow is the \textbf{magnitude} of the displacement while the \textbf{direction} of the displacement is the same as that of the arrow.

\subsection{Scalars and vectors}

A \textbf{scalar} is a quantity that is described by a \textbf{magnitude only}, while a \textbf{vector} is a quantity that is described by both its \textbf{magnitude and direction}.

Two vectors are said to be equal \textbf{if and only if} they have the same magnitude and direction.

\subsection{Vectors addition}

Vectors are quantities that have \textbf{magnitude} and \textbf{directions}. We cannot just consider their magnitudes because their \textbf{directions} are also important.

We say that we are adding individual displacements to find the \textbf{resultant displacement} in the last question. It is called the vector addition and method involved is called the \textbf{tip-to-tail} method.

\subsubsection{Tip-to-tail method of vector addition}

To add two vectors $\vec{A}$ and $\vec{B}$, firstly, we join the ``tip" of $\vec{A}$ to the ``tail" of $\vec{B}$. Then the sum $\vec{A} + \vec{B}$ is given by the vector represented by the arrow drawn from the ``tail" of $\vec{A}$ to the ``tip" of $\vec{B}$.

\subsection{Speed and velocity}

\textbf{Speed} is the rate of change of distance travelled. It is a scalar. Mathematically, the \textbf{average speed} of an object over a journey is defined as

$$ \text{average speed} = \frac{\text{total distance travelled}}{\text{time taken}} $$

\textbf{Velocity} is the rate of change of \textbf{displacement}. It is a \textbf{vector}.

We use the term \textbf{average speed} because the speed (\textbf{instantaneous speed}) of the object may change from time to time during the journey. For example, when a car is moving, the reading of its speedometer (which measures the \textbf{instantaneous} speed of the car) may change from time to time.

If the word ``speed" / ``velocity" is used alone, it usually refers to \textbf{instantaneous} speed / velocity.

The instantaneous speed is the average speed over a very short time interval.

In general, the average speed of a journey $\geqslant$ the magnitude of average velocity. Equality holds when \textbf{the direction of motion is constant}.

However, the instantaneous speed must be \textbf{equal} to the magnitude of average velocity. This is because when the time interval is small enough, the path is nearly a straight line segment.

\subsection{Uniform motion}

\textbf{Uniform motion} is a motion with constant velocity. This means its speed and its direction of motion are both constant. The motion is along a straight line.

If motion is uniform, the displacement of the object is \textbf{directly proportional} to the time of travel. The proportional constant is the \textbf{velocity} of the object.

Mathematically, the velocity ($v$), displacement ($s$) and time spent ($t$) are related by the following equation:

$$ s = vt $$

Since the displacement ($s$) is directly proportional to the time of travel ($t$), the graph of $s$ against $t$ is a \textbf{straight line} passing through the \textbf{origin}. The physical meaning of the slope of the graph is \textbf{velocity}.

The velocity-time graph ($v$-$t$ graph) of a uniform motion is a \textbf{horizontal} straight line. The area under the graph is equal to the \textbf{displacement} of the object.

\subsection{Acceleration}

\textbf{Acceleration} is the rate of change of velocity. It tells us how fast the \textbf{velocity} is changing (in both magnitude and direction).

The \textbf{average acceleration} of a motion can be calculated by the following equation.

$$ \vec{a} = \frac{\text{change of velocity}}{\text{time taken}} = \frac{\vec{v} - \vec{u}}{t} $$

where $\vec{v}$ is \textbf{final velocity}, $\vec{u}$ is \textbf{initial velocity} and $t$ is time taken. The unit of acceleration is $\SI{}{\metre\per\second\squared}$.

Acceleration is a \textbf{vector}.

\subsubsection{Change of velocity as a vector}

The change of velocity ($\Delta \vec{v} = \vec{v} - \vec{u}$) is also a vector.

If the speed is increasing, the direction of acceleration is \textbf{the same as} that of the velocity. If the speed is decreasing, the direction of acceleration is \textbf{opposite} to that of the velocity.

\subsubsection{Using sign convention to represent directions}

\textbf{Sign convention}

For \textbf{1-dimensional} motion, we can use the $+$/$-$ sign to represent the directions of vectors like displacements, velocities and accelerations. First of all, we have to define a positive direction. Then we can assign a ``$+$" to a vector quantity that is pointing towards the positive direction, and a ``$-$" to a vector quantity pointing towards the negative direction.

\subsection{Motion graphs}

The displacement-time graph ($s$-$t$ graph), velocity-time graph ($v$-$t$ graph) and acceleration-time graph ($a$-$t$ graph) are motion graphs that represent a certain motion.

The slope of an $s$-$t$ graph gives the velocity of the object.\\
The slope of a $v$-$t$ graph gives the acceleration of the object.\\
The area under a $v$-$t$ graph gives the displacement of the object.\\
The area under an $a$-$t$ graph gives the change of velocity of the object.

\subsection{Equations of uniformly accelerating motion}

For uniformly accelerating motion, the $v$-$t$ graph is a \textbf{straight line}. The area under the graph is a trapezium, which represents the \textbf{displacement} of the object.

Therefore, \begin{equation}
\label{eq:ua1}
s = \frac{1}{2}(u+v)t
\end{equation}

Sometimes, $\frac{1}{2}(u+v)$ is called the average velocity $\overline{v}$ of the motion. It must be stressed that $\overline{v} = \frac{1}{2}(u+v)$ is \textbf{only true} when the acceleration is uniform.

By definition, for uniform acceleration, 

\begin{equation}
\label{eq:ua2}
a = \frac{v-u}{t}
\end{equation}

From \eqref{eq:ua1} and \eqref{eq:ua2}, eliminating $t$, we have

\begin{equation}
\label{eq:ua3}
v^2 - u^2 = 2as
\end{equation}

Eliminating $v$, we have

\begin{equation}
\label{eq:ua4}
s = ut + \frac{1}{2}at^2
\end{equation}

Using the above four equations, we can solve problems involving uniform acceleration.

\subsection{One dimensional motion with changing direction}

If the direction of motion is changing, we have to adopt \textbf{sign conventions} to those vector quantities. The systematic way is summarized as follows:

\begin{enumerate}
	\item Define the positive direction. (Usually, the initial direction of motion is taken as positive.)
	\item Identify the given quantites and the unknowns that you have to find. Pay attention to the direction of the vector quantities and assign $+$/$-$ to them.
	\item Choose suitable equations of motion to solve the problem.
\end{enumerate}

\subsection{Motion under gravity}

When an object is moving freely in air, its acceleration is due to gravity only. Its accepted value is $\SI{9.81}{\metre\per\second\squared}$, pointing downwards.

It is found that the acceleration due to gravity is \textbf{independent} of the mass of the object.

This idea is first introduced by the famous scientist Galileo, who verified this result by dropping a heavy ball and a light ball from the top of the Leaning Tower of Pisa at the same time.

However, for some objects, like feather, parachute, hair, etc, the falling rate is much slower. This is due to the effect of \textbf{air resistance}. If a feather and a coin is dropped in vacuum, their acceleration is \textbf{the same}.