\section{Force and motion}

\subsection{Newton's first law of motion}

\textbf{Inertia} of a body is a measure of the \textbf{tendency} for it to remain at rest or to move at a uniform velocity. It resists any \textbf{change} in its state of \textbf{motion}.

A body of greater mass has a greater tendency to maintain its state of motion. That is, it has a greater \textbf{inertia}.

A bowling ball has a \textbf{greater} inertia than a table tennis ball.

\subsubsection{Newton's first law of motion}

A body continues in a state of rest or uniform motion along a straight line unless it is acted on by an external net force.

This law defines the concept of forces qualitatively. Force is something that can change the state of motion of a body. The state of motion of a body refers to its \textbf{velocity}.

\subsection{Force of interaction}

Force of interaction is used to describe the interaction between \textbf{two} objects - one of them exerts a force on the other. The following examples involve the existence of a force of interaction.

\begin{enumerate}
	\item The Earth is pulling an apple towards the ground.
	\item A man is pushing a heavy box.
	\item A rolling ball is slowed down by the friction due to the ground.
\end{enumerate}

Forces of interaction can be categorized into four fundamental types.

\begin{description}
	\item[Electromagnetic force] (e.g. all contact forces, tension, friction, electrostatic force)
	\item[Gravitational force] (i.e. weight)
	\item[Strong nuclear force] (appears only include nucleus, holding nucleons together)
	\item[Weak nuclear force] (governs radioactive decays and many other nuclear processes)
\end{description}

\subsection{Newton's third law of motion}

Newton's third law states that when an object $A$ exerts a force on another object $B$, then object $B$ will exert an equal but opposite force on object $A$. The two forces are called an \textbf{action-reaction pair}.

The action and the reaction exist at the \textbf{same} time. The two forces must be equal in \textbf{magnitude} but opposite in \textbf{directions}. Also, the two forces must act on \textbf{different} bodies.

\subsection{Examples of forces of interaction}

\subsubsection{Tension}

When a string is stretched, \textbf{tension} appears at every point along the string. \textbf{Tension at a point} refers to the action-reaction pair existing at that point.

If the string is light enough, we may assume that the tension at \textit{every} point on the string is the same. In this case, the term \textbf{tension along a string} or \textbf{tension of a string} refers to the tension at any one of the points on the string.

If the string breaks of loosens, the \textbf{tension} disappears immediately. Tension appears because of the bonding of the molecules in the string. Therefore, its basis nature is of electromagnetic type.

\subsubsection{Friction}

Friction arises between contacting surfaces that slides or tends to slide over each other. Friction always acts in a direction to \textbf{prevent} or to \textbf{oppose} the relative movement.

Suppose you pull a block on a table. If the pulling force $F$ is samll, it wil be balanced by the friction ($f$) on the block by the table, and the block does not move. The magnitude of $f$ is just equal to that of $F$. We call it \textbf{static friction}.

When $F$ gradually increases, $f$ also increases to prevent sliding. At a critical moment, $f$ attains its maximum value (called \textbf{limiting friction}). The block is still at rest because $f$ still can balance $F$.

However, when $F$ further increases, $f$ cannot balance $F$ anymore. The block starts to slide on the table surface. We call $f$ \textbf{kinetic friction} in this case. The magnitude of $f$ will keep at its maximum value (\textbf{limiting friction}).

\subsubsection{Normal contact force (normal reaction)}

\textbf{Normal direction} means the direction \textbf{perpendicular} to the surface. \textbf{Contact forces} arise when two objects touch each other. Friction is also an example of contact force. When you stand on the floor, a normal contact force (or normal reaction) is acting on you by the ground.

\subsubsection{Force of gravity}

The earth's gravity pulls everything towards its center. We can also call the force of gravity be \textbf{gravitational force} or \textbf{weight}. The magnitude of gravitational force on an object is directly proportional to its \textbf{mass}. The proportionality constant is called the gravitational field strength $g$, which describes the influence of a certain planet to a certain region. ($W = mg$) 

On the Earth's surface, $g = \SI{9.81}{\newton\per\kilo\gram}$.\\
But on the Moon's surface, the gravitational field is just about $\frac{1}{6}$ that on the Earth. ($g = \SI{1.62}{\newton\per\kilo\gram}$)

\subsection{Addition and resolution of forces}

Since forces are \textbf{vectors}, when we consider the net effect of several forces, we have to consider both the magnitudes and directions.

To find the net effect of two forces, $F_1$ and $F_2$, we have to do vector addition. There are two methods.

\subsubsection{Tip-to-tail method}

Without changing its direction, $F_1$ is shifted until its tail meets the tip of $F_2$. An arrow pointing from the tip of $F_2$ to the tail of $F_1$ is the resultant force.

\subsubsection{Parallelogram method}

By considering $F_1$ and $F_2$ as the adjacent sides of the parallelogram, an arrow pointing from the common tail of $F_1$ and $F_2$ to the opposite vertex of the parallelogram is the \textbf{resultant force}.

\subsubsection{Resolution of forces}

Forces can be added up using vector addition. On the other hand, we can \textbf{resolve} a force into two components. The \textbf{resolution of force} can be treated as the reverse process of vector addition.

Before resolving a force, usually we have to choose two perpendicular directions.

\subsubsection{Static equilibrium of particles}

If a particle remains stationary (static equilibrium), the net force acting on the particle must be zero.

To deal with problems of static equilibrium, first of all, we have to draw a force diagram on the specific object that is at rest. Then, the most systematic way is to resolve forces into two well chosen directions. Since the object is at rest, net force in the two directions are both zero.

\subsection{Newton's second law of motion}

Newton's second law of motion states that the acceleration of an object is \textbf{directly proportional} to the net force acting on it and \textbf{inversely proportional} to its mass. The direction of the acceleration is the same as that of the net force.

The unit of force if defined as Newton ($N$). One Newton of force is the force needed to cause an acceleration of $\SI{1}{\metre\per\second\squared}$ for an object of mass $\SI{1}{\kilo\gram}$.

Mathematically,

$$ \SI{1}{\newton} = \SI{1}{\kilo\gram\metre\per\second\squared} $$

The structure of Newtonian mechanics now is clear. The systematic way is as follows.

\begin{enumerate}
	\item Identify all forces of interaction acting \textbf{on} the object, by drawing force diagrams (or free body diagram).
	\item Using the information given, think qualitatively how the object is moving. (e.g. Is the direction of acceleration known?)
	\item Add all the forces up using vector addition, or by resolving components. The resultant force (or net force) is then found.
	\item Using the net force and the mass given, find the \textbf{acceleration} of the object using Newton's second law.
	\item Apply equations of motion to predict the motion of the object.
\end{enumerate}

\subsubsection{Weight and mass}

The weight of an object is \textbf{directly proportional} to its mass. The proportionality constant is called the \textbf{gravitational field strength} ($g$). Mathematically,

$$ W = mg $$

The gravitational field strength $g$ depends on the position where the object is placed. If it is near the Earth's surface, $g = \SI{9.81}{\newton\per\kilo\gram}$.

In the absense of air resistance, we know that the acceleration due to gravity for al objects is \textbf{the same}. We can explain it using the above relation. When an object is falling freely, the only force acting on it is its weight. By Newton's second law of motion,

$$ W = mg $$
$$ mg = ma $$
$$ a = g $$

The last equation points out that the acceleration of a free falling object is independent of its mass $m$. That is why sometimes, $g$ can also be called the acceleration due to gravity.

Weight and mass are different concepts. They are different in the following ways:

\begin{enumerate}
	\item Mass is a scalar, while weight is a vector.
	\item The unit of mass is $\SI{}{\kilo\gram}$, while the unit of weight is $\SI{}{\newton}$.
	\item Mass is a measure of \textbf{inertia}, while weight is a measure of \textbf{gravitational pull}.
	\item The mass of an object is \textbf{constant} everywhere, while its weight may \textbf{vary} from place to place. For instance, if the object is put on the surface of Moon, its mass is \textbf{unchanged} but its weight becomes \textbf{smaller}.
\end{enumerate}

\subsection{Typical questions on Newtonian mechanics}

\subsubsection{Motion on an inclined plane}

A block of mass $m$ is placed on a \textit{smooth} inclined plane with an inclined angle $\theta$.

\begin{itemize}
	\item Draw the force diagram for the block.
	\item If the motion is along the plane, it is convenient to find the net force by resolving the components along 
	\begin{inparaenum}
		\item the direction of the plane; and
		\item the direction perpendicular to the plane
	\end{inparaenum}
	\item Results:\\
	\begin{tabular}{|l|l|}
		\hline
		Normal reaction acting on the block & $ mg \cos \theta $\\
		Net force acting on the block & $ mg \sin \theta $\\
		Acceleration of the block & $ \frac{F_\text{net}}{m} = g \sin \theta $\\
		\hline
	\end{tabular}
\end{itemize}

If the plane is rough, the block may be at rest or in motion. If it is at rest, the (static) friction \textbf{balances} the weight component along the plane. ($f = mg \sin \theta$) If the block slide down with acceleration, net force is $mg \sin \theta - f$, acceleration = $g \sin \theta - \frac{f}{m}$.

\subsubsection{Motion inside a lift}

A person is standing on a weighing machine in the lift. The reading of the machine is actually the force acting on the \textbf{machine} by the person. However, by Newton's third law, the reading is also equal to the force acting on the \textbf{person} by the \textbf{machine}. The reading is \textbf{not} exactly the weight of the person.

When the lift is at rest, or moving at uniform velocity, the net force acting on the person is zero. Therefore, the normal reaction is equal to the weight of the person. The reading of the balance is \textbf{equal to} the weight of the person.

But when the lift is accelerating, net force acting on him is not zero. The reading is \textbf{not equal} to his weight.

Our \textbf{feeling of weight} comes from the \textbf{normal reaction} by the ground. If the lift is accelerating downwards, the balance reading is smaller than our weight. We feel that our weight is smaller. But this is only a feeling. In fact, our weight is constant.

If the cable supporting the lift breaks suddenly, the downward acceleration of the lift is $\SI{9.81}{\metre\per\second\squared}$. The person is said to be in a state of \textbf{weightlessness}.

\subsubsection{Sky-diving}

When a skydiver is falling in air, air resistance may appear. The magnitude of air resistance is directly proportional to his speed.

As the skydiver just leaves an aircraft, his speed is \textbf{slow}. The air resistance acting on him is therefore \textbf{small}. The net force acting on him is nearly equal to his \textbf{weight}.

When the skydiver gains speed, the air resistance acting on him \textbf{increases}. The net force acting him \textbf{decreases}. His acceleration becomes \textbf{smaller}.

At a certain instant, the air-resistance \textbf{increases} to a value just balance his weight. The skydiver falls at a \textbf{constant} velocity. It is called the terminal velocity.

When the parachute is opened, the air resistance suddenly greatly \textbf{increases} and is larger than his weight. The net force is pointing \textbf{upwards}. The skydiver suddenly slow down quickly and can land safely.

\subsubsection{Connected bodies}

Sometimes there are two or more bodies interacting and moving together. When drawing force diagrams, we need to be very clear what of \textbf{focus object} of the force diagram is.